var fs = require("fs");
var buf = new Buffer(1024);
/*Open a File
===============================================================================================
Flag   |  Description
===============================================================================================
r	   |  Open file for reading. An exception occurs if the file does not exist.
r+	   |  Open file for reading and writing. An exception occurs if the file does not exist.
rs	   |  Open file for reading in synchronous mode.
rs+	   |  Open file for reading and writing, asking the OS to open it synchronously. See notes for 'rs' about using this with caution.
w	   |  Open file for writing. The file is created (if it does not exist) or truncated (if it exists).
wx	   |  Like 'w' but fails if the path exists.
w+	   |  Open file for reading and writing. The file is created (if it does not exist) or truncated (if it exists).
wx+	   |  Like 'w+' but fails if path exists.
a	   |  Open file for appending. The file is created if it does not exist.
ax	   |  Like 'a' but fails if the path exists.
a+	   |  Open file for reading and appending. The file is created if it does not exist.
ax+	   |  Like 'a+' but fails if the the path exists.
===============================================================================================
*/
console.clear();
console.log("-------------------------------------------");
fs.open('test.txt', 'r+', (err, fd) => {
    if (err) {
        return console.error(err);
    }
    console.log("File opened successfully!");
    console.log("-------------------------------------------");
});

/*Get File Information */
fs.stat('test.txt', (err, stats) => {
    if (err) {
        return console.error(err);
    }
    console.log(stats);
    console.log("isFile ? ", stats.isFile());
    console.log("isDirectory ? " + stats.isDirectory());
    console.log("isBlockDevice ? " + stats.isBlockDevice());
    console.log("isCharacterDevice ? " + stats.isCharacterDevice());
    console.log("isSymbolicLink ? " + stats.isSymbolicLink());
    console.log("isFIFO ? " + stats.isFIFO());
    console.log("isSocket ? " + stats.isSocket());
    console.log("-------------------------------------------");
});

/*Writing a File : fs.writeFile(filename, data[, options:{encoding:'utf8', mode:0666, flag : 'w'}], callback)*/
fs.writeFile('test.txt', 'This is sample written by HK', (err) => {
    if (err) {
        return console.error(err);
    }
    console.log("Data written successfully");
    console.log("-------------------------------------------");

    /*Reading a File : fs.read(fd, buffer, offset, length, position, callback) */
    fs.open('test.txt', 'r+', (err, fd) => {
        if (err) {
            return console.error(err);
        }
        console.log("File opened successfully!");
        console.log("Going to read the file");
        fs.read(fd, buf, 0, buf.length, 0, (err, bytes) => {
            if (err) {
                console.log(err);
            }
            console.log(bytes + " bytes read");
            if (bytes > 0) {
                console.log(buf.slice(0, bytes).toString());
            }
            console.log("-------------------------------------------");
            /* Closing a File : fs.close(fd, callback) */
            fs.close(fd, function (err) {
                if (err) {
                    console.log(err);
                }
                console.log("File closed successfully.");
                console.log("-------------------------------------------");
                truncateFile();
            });
        });
    });
});

/*Truncate a File : fs.ftruncate(fd, len, callback) */
var truncateFile = () => {
    fs.open('test.txt', 'r+', function (err, fd) {
        if (err) {
            return console.error(err);
        }
        console.log("File opened successfully!");
        console.log("Going to truncate the file after 10 bytes");
        fs.ftruncate(fd, 10, function (err) {
            if (err) {
                console.log(err);
            }
            console.log("File truncated successfully.");
            console.log("Going to read the same file");
            fs.read(fd, buf, 0, buf.length, 0, function (err, bytes) {
                if (err) {
                    console.log(err);
                }
                if (bytes > 0) {
                    console.log(buf.slice(0, bytes).toString());
                }
                fs.close(fd, function (err) {
                    if (err) {
                        console.log(err);
                    }
                    console.log("File closed successfully.");
                    console.log("-------------------------------------------");
                    deleteFile();
                });
            });
        });
    });
}

/*Delete a File : fs.unlink(path, callback) */
var deleteFile = () => {
    fs.unlink('test.txt', function (err) {
        if (err) {
            return console.error(err);
        }
        console.log("File deleted successfully!");
        console.log("-------------------------------------------");
        createDirectory();
    });
}

/*Create a Directory : fs.mkdir(path[, mode = 0777], callback)*/
var createDirectory = () => {
    fs.mkdir('test', function (err) {
        readDirectory();
        if (err) {
            return console.error(err);
        } else {
            console.log("'test' Directory created successfully!");
        }
        console.log("-------------------------------------------");

    });
}

/*Read a Directory : fs.readdir(path, callback)*/
var readDirectory = () => {
    fs.readdir("test", function (err, files) {
        if (err) {
            return console.error(err);
        }
        console.log("Reading 'test' directory successfully!");
        files.forEach(function (file) {
            console.log(file);
        });
        console.log("-------------------------------------------");
        removeDirectory();
    });
}

/*Remove a Directory : fs.rmdir(path, callback)*/
var removeDirectory = () => {
    fs.rmdir("test", function (err) {
        if (err) {
            return console.error(err);
        }
        console.log("Going to read directory /test");
        fs.readdir("test", function (err, files) {
            if (err) {
                return console.error(err);
            }
            files.forEach(function (file) {
                console.log(file);
            });
            console.log("-------------------------------------------");
        });
    });

}