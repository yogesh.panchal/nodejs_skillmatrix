console.clear();
/*Creating Buffers*/
var buf = new Buffer(26); // [0,0,0,.... upto 26]
var buf2 = new Buffer([97, 98, 99, 100, 101]); //abcde
var buf3 = new Buffer("My Buffer", "utf-8");

/*Writing to Buffers*/
var len = buf.write("My Buffer Write Demo");
console.log("Octets written : "+  len);

/*Reading from Buffers*/
for (var i = 0 ; i < 26 ; i++) {
  buf[i] = i + 97;
}
console.log("Reading from Buffers :: 1 :: ascii) =>", buf.toString('ascii'));       // outputs: abcdefghijklmnopqrstuvwxyz
console.log("Reading from Buffers :: 2 :: ascii 0,5) =>", buf.toString('ascii',0,5));   // outputs: abcde
console.log("Reading from Buffers :: 3 :: utf8 0,5) =>", buf2.toString('utf8',0,5));    // outputs: abcde
console.log("Reading from Buffers :: 4 :: undefined 0,5) =>", buf.toString(undefined,0,5)); // encoding defaults to 'utf8', outputs abcde

/*Convert Buffer to JSON*/
var json = buf.toJSON(buf);
console.log("Convert Buffer to JSON :: => ",json);

/*Concatenate Buffers*/
buf = new Buffer("Hi, I'm ");
buf2 = new Buffer("Harikrushna Moradiya");
buf3 = Buffer.concat([buf,buf2]);
console.log("Content: =>" + buf3.toString());

/*Compare Buffers*/
buf = new Buffer('Harikrushna');
buf2 = new Buffer('Moradiya');
result = buf.compare(buf2);

if(result < 0) {
   console.log(buf +" comes before " + buf2);
}else if(result == 0){
   console.log(buf +" is same as " + buf2);
}else {
   console.log(buf +" comes after " + buf2);
}

/*Copy Buffer*/
buf3 = new Buffer(10);
buf.copy(buf3,2,2,5);
console.log("Copy Buffer: (buf to buff3) => ", buf.toString(),buf3.toString());
console.log("Copy Buffer: (buf to buff3) => ", buf.toJSON(buf),buf3.toJSON(buf3));

/* Slicing a buffer */
buf2 = buf.slice(0,9);
console.log("Slicing: => " ,buf.toString(), buf2.toString());

/*Buffer Length*/
console.log(`buf Length '${buf.toString()}' is :`, buf.length);


/*Method isEncoding(encoding)*/
console.log(`Buffer.isEncoding('ascii') is :`, Buffer.isEncoding('ascii'));
console.log(`Buffer.isEncoding('utf8') is :`, Buffer.isEncoding('utf8'));
console.log(`Buffer.isEncoding(undefined) is :`, Buffer.isEncoding(undefined));
console.log(`Buffer.isEncoding('json') is :`, Buffer.isEncoding('json'));
console.log(`Buffer.isEncoding(12345) is :`, Buffer.isEncoding(12345));

/*Method :: isBuffer(obj)*/
console.log(`Buffer.isBuffer(buf2) is Buffer :`, Buffer.isBuffer(buf));
console.log(`Buffer.isBuffer('') is Buffer :`, Buffer.isBuffer(''));

/*Method :: byteLength(string[, encoding])*/
console.log(`Buffer.byteLength("Harikrushna", 'ascii') length is :`, Buffer.byteLength("Harikrushna", 'ascii'));
console.log(`Buffer.byteLength("Harikrushna", 'utf8') length is :`, Buffer.byteLength("Harikrushna", 'utf8'));
console.log(`Buffer.byteLength("Harikrushna", undefined) length is :`, Buffer.byteLength("Harikrushna", undefined));

/*Method :: concat(list[, totalLength])*/
console.log(`Concat => `, Buffer.concat([buf,buf2,buf3],100).toString());

/*Method :: compare(buf1, buf2)*/
result = Buffer.compare(buf,buf2)
if(result < 0) {
   console.log(buf +" comes before " + buf2);
}else if(result == 0){
   console.log(buf +" is same as " + buf2);
}else {
   console.log(buf +" comes after " + buf2);
}



