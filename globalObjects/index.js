var fs = require('fs')
console.clear();
/*__filename*/
console.log( __filename );

/*__dirname*/
console.log( __dirname );

/*setTimeout*/
var timeout = setTimeout(()=>{
	console.log("called after 1 Sec.");
	/*clearTimeout*/
	clearTimeout(timeout);
}, 1000)


/*setInterval*/
var interval = setInterval(()=>{
	console.log("called after 1 Sec.");
	/*clearInterval*/
	clearTimeout(interval);
}, 1000)

/*console Object*/
console.log("Example of : console.log")
console.info("Example of : console.info")
console.error("Example of : console.error")
console.warn("Example of : console.warn")
console.dir({mysample: "Example of : console.dir"})
console.time("My Time Label")
console.timeEnd("My Time Label")
//console.trace("Example of : console.trace")
var myObj = { firsname : "Harikrushna", lastname : "Moradiya"};
console.assert(true, myObj);


/*Process Events*/
/*exit*/
process.on('exit',(code) => {
	setTimeout(() => {
		console.log("This will not run");
	}, 0);
	
   	console.log('About to exit with code:', code); 
});

/*rejectionHandled & unhandledRejection*/
const unhandledRejections = new Map();
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at:', p, 'reason:', reason);
});
process.on('rejectionHandled', (promise) => {
  unhandledRejections.delete(promise);
});


/*uncaughtException*/
process.on('uncaughtException', (err) => {
  fs.writeSync(1, `Caught exception: ${err}\n`);
});

/*warning*/
process.on('warning', (warning) => {
  console.warn(warning.name);  
  console.warn(warning.message); 
  console.warn(warning.stack); 
});

/*Process Methods*/
console.log('arch ==>',process.arch);
console.log('argv ==>',process.argv);
console.log('argv0 ==>',process.argv0);
console.log('channel ==>',process.channel);
console.log('config ==>',process.config);
console.log('connected ==>',process.connected);
console.log('cpuUsage ==>',process.cpuUsage());
console.log('cwd ==>',process.cwd());
console.log('debugPort ==>',process.debugPort);
console.log('env ==>',process.env);
console.log('execArgv ==>',process.execArgv);
console.log('execPath ==>',process.execPath);
console.log('getegid ==>',process.getegid());
console.log('geteuid ==>',process.geteuid());
console.log('getgid ==>',process.getgid());
console.log('getgroups ==>',process.getgroups());
console.log('getuid ==>',process.getuid());
console.log('mainModule ==>',process.mainModule);
console.log('memoryUsage ==>',process.memoryUsage());
console.log('noDeprecation ==>',process.noDeprecation);
console.log('pid ==>',process.pid);
console.log('platform ==>',process.platform);
console.log('ppid ==>',process.ppid);
console.log('release ==>',process.release);
console.log('title ==>',process.title);
console.log('version ==>',process.version);
console.log('versions ==>',process.versions);

console.log('start');
process.nextTick(() => {
  console.log('nextTick callback');
});
console.log('scheduled');

// process.exit(1);



