var mqtt = require('mqtt');
var fs = require('fs');
var path = require('path');
var cryption = require('./cryption');

 
const topic = 'presence';
// var lastWill = { will: { topic: topic,
//     payload: 'Device Offline' , 
//     },
//     clientId: '1',
//     port: 1883,
//     clean: false
// };
//var client  = mqtt.connect('mqtt://localhost',lastWill);
var client = mqtt.connect('mqtts://localhost:8883' , {
    username: 'test',
    password: 'test',
    keepalive: 60,
    clean: true,
    clientId: '1',
    qos: 0,
    key: fs.readFileSync(path.resolve('/home/test/nodejs/idp_test/nodejs/mqtt-examples/sample/certs/client.key')),
    cert: fs.readFileSync(path.resolve('/home/test/nodejs/idp_test/nodejs/mqtt-examples/sample/certs/client.crt')),
    ca: fs.readFileSync(path.resolve('/home/test/nodejs/idp_test/nodejs/mqtt-examples/sample/certs/ca.crt')),
    rejectUnauthorized: true,
    checkServerIdentity: function (host, cert) {
        if (host != cert.subject.CN) {
            console.log("host and cert with are incorrect");
            return 'Incorrect server identity';// Return error in case of failed checking.
        } else {
            console.log("host and cert with correct");
        }
    }
});
var options={retain:false, qos: 1};

client.on('connect', function () {
    console.log("Connection eastablished! and Start publishing messages!!!!!");
});

client.on('error', function(err) {
    console.log(err);
});

setInterval(publishMessage,1000);
function publishMessage() {
    console.log('Sending Message: `Hello mqtt` on topic: ' + topic);
    let encodedString = cryption.encrypt("1345","Hello mqtt");
    client.publish(topic, encodedString ,options);
}
